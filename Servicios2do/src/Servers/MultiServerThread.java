package Servers;

import java.net.*;
import java.io.*;
import java.util.Date;
import static Servers.ClientFullDuplex.conexionCliente;

public class MultiServerThread extends Thread {
   private Socket socket = null;
   private int nu=0,cont=0,i=0;
      private String[] ServicesDirectory1= {"con","may","min","IMECA"};
   private String[] ServicesDirectory2= {"may","inv","IMECA"};
    private String[] ServicesDirectory3= {"min","txt","bin","IMECA"};


   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {
     try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
          nu=1;
        
	     while((lineIn = entrada.readLine()) != null){
                
            System.out.println("Received: "+lineIn);
            escritor.flush();     
            String[] partes = lineIn.split("#");
            cont=0;
            
            
            if(lineIn.equals("FIN")){
                ServerMultiClient.NoClients--;
                break;
            }
           else if(lineIn.equals("#")){
               escritor.println("No. de clientes: "+ServerMultiClient.NoClients);
               escritor.flush();
               }
           else if(lineIn.equals("IMECA")){
                Scraping_allPost ime=new Scraping_allPost();
                escritor.println("#r:imc#"+ime.IMECA()+"#");
                escritor.flush(); 
           }
           else{
           nu=Integer.parseInt(partes[2]);
           }
  
            System.out.println("Received: "+lineIn);
            escritor.flush();
            String respuesta="",respuesta2="";
            int aux=0,i=0,rect=0,poop;
           
           //System.out.println("Contador: "+cont+"  Numero: "+nu+"\n");
            String cadena="";
            //System.out.println("Cadena: "+cadena);    

              switch(partes[1]){
                  
                /* case "inv":
                     
                     for(aux=3;aux<=nu+2;aux++)
                    {
                        cadena=partes[aux];
                        StringBuilder builder=new StringBuilder(cadena);
                        respuesta2=builder.reverse().toString();
                        respuesta=respuesta+"#"+respuesta2;
                    }
              escritor.println("#r:inv#"+nu+respuesta+"#");
              
            break;*/
            
              
              case "con": 
                for(aux=3;aux<=nu+2;aux++)
                    {
                        respuesta=respuesta+partes[aux];
                    }
                    escritor.println("#r:con#"+nu+"#"+respuesta+"#");
                    escritor.flush();
                break;
/*
                */case "may":
                    
                    for(aux=3;aux<=nu+2;aux++)
                    {
                        cadena=partes[aux];
                        respuesta2=cadena.toUpperCase();
                        respuesta=respuesta+"#"+respuesta2;
                    }
             escritor.println("#r:may#"+nu+respuesta+"#");
              
             break;
             
            
                case "min":
             
              for(aux=3;aux<=nu+2;aux++)
                    {
                        cadena=partes[aux];
                        respuesta2=cadena.toLowerCase();
                        respuesta=respuesta+"#"+respuesta2;
                    }

             escritor.println("#r:min#"+nu+"#"+respuesta2+respuesta+"#");
         
             
            break;
              
               /* case "txt":
              for(aux=3;aux<=nu+2;aux++)
                    {
                        cadena=partes[aux];
                     
                        int numero=Integer.parseInt(cadena);
                        int lon=cadena.length();
                        cadena=num_txt(numero,lon);
                        respuesta2=cadena;
             
                        respuesta=respuesta+"#"+respuesta2;
                    }
             
             escritor.println("#r:txt#"+nu+respuesta+"#");
           
            break;
              
              
                case "bin":
             
             
             for(aux=3;aux<=nu+2;aux++)
                    {
                        cadena=partes[aux];
                        int  numero=Integer.parseInt(cadena);
               
           String binario = "";
        if (numero > 0) {
            while (numero > 0) {
                if (numero % 2 == 0) {
                    binario = "0" + binario;
                } else {
                    binario = "1" + binario;
                }
                numero = (int) numero / 2;
            }
        } else if (numero == 0) {
            binario = "0";
        } else {
            binario = "No se pudo convertir el numero. Ingrese solo números positivos";
        }
        respuesta2=binario;
                       
             
                        respuesta=respuesta+"#"+respuesta2;
                    }
         escritor.println("#r:bin#"+nu+respuesta+"#");
       
        break; */
                
                default:
                    //escritor.println("No se detecto el codigo en Server 1");
                    poop = ServicesDirectory2.length-1;
                    System.out.println("No se detecto el codigo en Server 1");
                    /*for(int u = 0;u <= poop;u++)
                    {
                        if(partes[1].equals(ServicesDirectory2[u])) {
                            rect = 1;
                            //System.out.println(rect);
                        } 
                        else if(partes[1].equals(ServicesDirectory3[u])) {
                            rect = 2;
                        //    System.out.println(rect);
                        }
                    }*/
                    rect=ControlServer.DefinedGlobal(partes[1]);
                    if(rect==1)
                    {
                        System.out.println("Detectado en Server 2; haciendo conexion");
                        //System.out.println("Puerto: 12355");
                        String[] solicitud = {ControlServer.getDataConnection(rect),lineIn};
                        respuesta = conexionCliente(solicitud);
                    
                        rect=0; 
                        escritor.println("Respuesta desde el servidor 2:"+respuesta);
                    }
                    else if(rect==2)
                    {
                        System.out.println("Detectado en Server 3; haciendo conexion");
                        //System.out.println("Puerto: 12356");
                        String[] solicitud = {ControlServer.getDataConnection(rect),lineIn};
                        respuesta = conexionCliente(solicitud);
                     
                        rect=0; 
                        escritor.println("Respuesta desde el servidor 3:"+respuesta);
                    }
                    else
                    {
                        escritor.println("El comando no se encontro en ningun Server Disponible");
                    }
                    break;
            }
             escritor.flush();
          
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   
} 
