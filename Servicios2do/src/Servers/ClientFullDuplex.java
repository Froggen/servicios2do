package Servers;

import java.net.*; 
import java.io.*; 
public class ClientFullDuplex { 
	public static String conexionCliente (String[] argumentos)throws IOException{ 
		Socket cliente = null; 
		PrintWriter escritor = null; 
		String DatosEnviados = null; 
		BufferedReader entrada=null;
		
		String maquina,orden; 
		int puerto,tipoCliente = 0;
                maquina = "localhost";
		BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in));
		if (argumentos.length != 2){ 
			puerto = 12345; //SERVER 1
                        //puerto = 12346;  //Server 2
			System.out.println ("Conectado a " + maquina + " en puerto: " + puerto);
                        tipoCliente=0;
		} 
		else{ 
			Integer pasarela = new Integer (argumentos[0]);
			puerto = pasarela.parseInt(pasarela.toString()); 
			System.out.println ("Conectado a " + maquina + " en puerto: " + puerto); 
                        DatosEnviados = argumentos[1]; 
                        tipoCliente = 1;
		} 
		try{ 
			cliente = new Socket (maquina,puerto); 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			System.exit (0); 
		}
		try{ 
			escritor = new PrintWriter(cliente.getOutputStream(), true);
			entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));
 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			cliente.close(); 
			System.exit (0); 
		} 
		String line="";
		
		System.out.println("Conectado con el Servidor. Listo para enviar datos...");
		
		do{ 
                    if(tipoCliente==0)
                    {
                        DatosEnviados = DatosTeclado.readLine(); 
                        escritor.println (DatosEnviados); 
                        line = entrada.readLine();
                        System.out.println(line);
                    }
                    if(tipoCliente==1)
                    {
                        escritor.println (DatosEnviados); 
                        line = entrada.readLine();
                        DatosEnviados = "FIN";
                    }
                        
		}while (!DatosEnviados.equals("FIN")); 
	
		System.out.println ("Finalizada conexion con el servidor"); 
		try{ 
			escritor.close();
		}catch (Exception e){}
                tipoCliente=0;
	return line;
        }
        
        public static void main(String[] argumentos) throws IOException {
            String[] hola = {"hola"};
            String adios  = conexionCliente(hola);
        }
} 

